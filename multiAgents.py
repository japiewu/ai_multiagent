# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from time import sleep
from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
  """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
  """


  def getAction(self, gameState):
    """
    You do not need to change this method, but you're welcome to.

    getAction chooses among the best options according to the evaluation function.

    Just like in the previous project, getAction takes a GameState and returns
    some Directions.X for some X in the set {North, South, West, East, Stop}
    """
    # Collect legal moves and successor states
    legalMoves = gameState.getLegalActions()

    # Choose one of the best actions
    scores = [self.evaluationFunction(gameState, action) for action in legalMoves]

    bestScore = max(scores)
    bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
    chosenIndex = random.choice(bestIndices) # Pick randomly among the best

    "Add more of your code here if you want to"

    return legalMoves[chosenIndex]

  def evaluationFunction(self, currentGameState, action):
    """
    Design a better evaluation function here.

    The evaluation function takes in the current and proposed successor
    GameStates (pacman.py) and returns a number, where higher numbers are better.

    The code below extracts some useful information from the state, like the
    remaining food (oldFood) and Pacman position after moving (newPos).
    newScaredTimes holds the number of moves that each ghost will remain
    scared because of Pacman having eaten a power pellet.

    Print out these variables to see what you're getting, then combine them
    to create a masterful evaluation function.
    """
    # Useful information you can extract from a GameState (pacman.py)
    successorGameState = currentGameState.generatePacmanSuccessor(action)
    # sleep(1)
    newPos = successorGameState.getPacmanPosition()
    oldFood = currentGameState.getFood()
    newGhostStates = successorGameState.getGhostStates()
    # print "newGhostStates: ",newGhostStates.getPosition()
    newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
    
    "*** YOUR CODE HERE ***"
    newGoPos = successorGameState.getGhostPosition(1)
    nowPos = currentGameState.getPacmanPosition()
    nowGoPos = currentGameState.getGhostPosition(1)

    score = 0
    list = []
    list.append(manhattanDistance(newPos,newGoPos))
    list.append(manhattanDistance(nowPos,newGoPos))
    list.append(manhattanDistance(newPos,nowGoPos))
    list.append(manhattanDistance(newPos,nowGoPos))

    for ele in list:
      if ele == 0:
        return -2e9
      if list[0] == 1 :
        score = -1e9

    discore = -1e9
    for food in oldFood.asList():
      dis = manhattanDistance(newPos,food)
      if -dis > discore:
         discore = -dis
    return discore + score

def scoreEvaluationFunction(currentGameState):
  """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
  """
  return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
  """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
  """

  def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
    self.index = 0 # Pacman is always agent index 0
    self.evaluationFunction = util.lookup(evalFn, globals())
    self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
  """
    Your minimax agent (question 2)
  """

  def getAction(self, gameState):
    """
      Returns the minimax action from the current gameState using self.depth
      and self.evaluationFunction.

      Here are some method calls that might be useful when implementing minimax.

      gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

      Directions.STOP:
        The stop direction, which is always legal

      gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

      gameState.getNumAgents():
        Returns the total number of agents in the game
    """
    "*** YOUR CODE HERE ***"
    tmp = self.minmax(gameState,'max',self.depth,0)
    # print "tmp:",tmp
    return tmp[1]

    # util.raiseNotDefined()
  def minmax(self,currentgameState,player,depth,count):
    actions = []
    actions = currentgameState.getLegalActions(count)
    if depth == 0 or actions == []:
      return (self.evaluationFunction(currentgameState),)
    
    if 'Stop' in actions and player == 'max':
      actions.remove('Stop')
    scores = []
    for act in actions:
      successorGameState = currentgameState.generateSuccessor(count,act)
      if player == 'max':  
        tmp = self.minmax(successorGameState,'min',depth,count+1)
      elif count < currentgameState.getNumAgents()-1:
        tmp = self.minmax(successorGameState,'min',depth,count+1)
      else:
        tmp = self.minmax(successorGameState,'max',depth-1,0)
      
      scores.append((tmp[0],act))
    
    scores.sort()
    if player == 'max':
      return max(scores)
    else:
      return min(scores)




class AlphaBetaAgent(MultiAgentSearchAgent):
  """
    Your minimax agent with alpha-beta pruning (question 3)
  """

  def getAction(self, gameState):
    """
      Returns the minimax action using self.depth and self.evaluationFunction
    """
    "*** YOUR CODE HERE ***"    
    return self.minmax(gameState,'max',self.depth,0,-1e9,1e9)[1]
    # return self.alphabeta(gameState,'max',self.depth,0,-1e9,1e9)[1]

  def minmax(self,currentgameState,player,depth,count,alpha,beta):
    
    actions = []
    actions = currentgameState.getLegalActions(count)
    if depth == 0 or actions == []:
      return (self.evaluationFunction(currentgameState),)
    
    if 'Stop' in actions and player == 'max':
      actions.remove('Stop')
    scores = []
    for act in actions:
      successorGameState = currentgameState.generateSuccessor(count,act)
      if player == 'max':
        v = -1e9
        v = max(v,self.minmax(successorGameState,'min',depth,count+1,alpha,beta)[0])
        if v >= beta: 
          return (v,act)
        alpha = max(alpha,v)        
        # scores.append((alpha,act))
      else:
        v = 1e9
        if count < currentgameState.getNumAgents()-1:
          v = min(v,self.minmax(successorGameState,'min',depth,count+1,alpha,beta))
        else:
          v = min(v,self.minmax(successorGameState,'max',depth-1,0,alpha,beta))
        if alpha >= v:
          return (v,act)
        beta = min(beta,v)
        # scores.append((beta,act))
      
      scores.append((v,act))
    
    scores.sort()
    if player == 'max':
      return max(scores)
    else:
      return min(scores)    



  def alphabeta(self,currentgameState,player,depth,count,alpha,beta):
    print "agent: ",count,'alpha: ',alpha,' beta: ',beta
    actions = []
    actions = currentgameState.getLegalActions(count)
    if actions == [] or depth == 0:
      # print "self.evaluationFunction(currentgameState): ",self.evaluationFunction(currentgameState)
      return (self.evaluationFunction(currentgameState),)

    # if 'Stop' in actions and player == 'max':
    #   actions.remove('Stop')
    scores = []
    
    if player == 'max' :
      v = -1e9
      get = (-1e9,)
      for act in actions:
        successorGameState = currentgameState.generateSuccessor(count,act)
        get = self.alphabeta(successorGameState,'min',depth,count+1,alpha,beta)
        # print 'maxNode'
        print 'get:',get
        # print 'v: ',v
        
        # if get[0] > alpha:
        #   alpha = get[0]
        # if alpha >= beta:
        #   return (alpha,act)

        print 'alpha: ',alpha
        print 'beta: ',beta
        if get[0] > v:
          v = get[0]          
        if v >= beta: 
          return (v,act)
        if v > alpha:
          alpha = v

        # print 'maxNode2'
        # print 'get:',get
        # print 'alpha: ',alpha
        # print 'beta: ',beta
        # print 'v: ',v        
        # print 'scores.append((alpha,act)): ',(alpha,act)
        scores.append((alpha,act))

      scores.sort()
      print 'maxscores: ',scores
      print 'max(scores): ',max(scores)
      # return (alpha,act)
      return max(scores)
    else:
      v = 1e9
      get = (1e9,)
      for act in actions:
        successorGameState = currentgameState.generateSuccessor(count,act)
        if count < currentgameState.getNumAgents()-1:
          get = self.alphabeta(successorGameState,'min',depth,count+1,alpha,beta)
        else:
          get = self.alphabeta(successorGameState,'max',depth-1,0,alpha,beta)
            
        if get[0] < v:
          v = get[0]
        if alpha >= v:
          return (v,act)
        # print 'minNode'
        print 'min get:',get
        print 'min alpha: ',alpha
        print 'min beta: ',beta
        # print 'v: ',v
        # print 'scores.append((beta,act))',(beta,act)
        if v < beta :
          beta = v

        scores.append((beta,act))

      scores.sort()
      print 'minscore: ',scores
      print 'min(scores): ',min(scores)
      print 
      # return (beta,act)
      return min(scores)


class ExpectimaxAgent(MultiAgentSearchAgent):
  """
    Your expectimax agent (question 4)
  """

  def getAction(self, gameState):
    """
      Returns the expectimax action using self.depth and self.evaluationFunction

      All ghosts should be modeled as choosing uniformly at random from their
      legal moves.
    """
    "*** YOUR CODE HERE ***"      
    tmp = self.expectMiniMax(gameState,'max',self.depth,0)
    return tmp[1]

  def expectMiniMax(self,currentgameState,player,depth,count):
    actions = []
    actions = currentgameState.getLegalActions(count)
    if actions == [] or depth == 0:
      return (self.evaluationFunction(currentgameState),)

    if 'Stop' in actions and player == 'max':
      actions.remove('Stop')
    scores = []
    for act in actions:
      successorGameState = currentgameState.generateSuccessor(count,act)
      if player == 'max':
        tmp = self.expectMiniMax(successorGameState,'min',depth,count+1)
      elif count < currentgameState.getNumAgents()-1:
        tmp = self.expectMiniMax(successorGameState,'min',depth,count+1)
      else:
        tmp = self.expectMiniMax(successorGameState,'max',depth-1,0)

      scores.append((tmp[0],act))
  
    if player == 'max':
      bestScore = max(scores)
      bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
      chosenIndex = random.choice(bestIndices)
      return scores[chosenIndex]
    else:
      add = 0
      for ele in scores:
        add += float(ele[0])
      return (add/(len(actions)),)

foodgoal = (0,0)
def betterEvaluationFunction(currentGameState):
  """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
      1.Use bfs to search the nearst food from where the pacman is on
        the 'NOW' time. And the score about food is 1/distance.
        This reveals that the closer food is, the higher score is.
      2.see capsule as food
      3.if the ScaredTime sub distance between pacman and ghost is larger than a constant,
        the ghostScore is 100/distance 
      4.Add the score of above and the getScore function of the state,which
        is considered to avoid the ghost, as the final score.

  """
  "*** YOUR CODE HERE ***"  
  oldFood = currentGameState.getFood()
  GhostStates = currentGameState.getGhostStates()  
  
  for a in currentGameState.getCapsules():
    oldFood[a[0]][a[1]] = True
  Pos = currentGameState.getPacmanPosition()  

  gScore = 0
  ScaredTimes = [ghostState.scaredTimer for ghostState in GhostStates]
  minDis = 1e9
  for agentIndex in range(0,currentGameState.getNumAgents()-1):
    if not ScaredTimes[agentIndex] == 0:
      gDis = bfs(Pos,currentGameState,'ghost',agentIndex+1)
      if ScaredTimes[agentIndex] - gDis > 3 and gDis < minDis :
        minDis = gDis
        gScore = 100.0/minDis

  
  dis = bfs(Pos,currentGameState,'food',0)
  return (1.0/dis) + currentGameState.getScore() + gScore


def bfs(Pos,currentGameState,End,agentIndex):
  visted = [Pos]
  queue = [(Pos,0)]
  around = [(0,1),(1,0),(0,-1),(-1,0)]
  while(1):
    if queue:
      node = queue.pop(0) 
      if End=='food' and currentGameState.hasFood(node[0][0],node[0][1]):
        return node[1]
      elif End == 'capsule':
        for a in currentGameState.getCapsules():
          if node[0][0]==a[0] and node[0][1] == a[1]:
            return node[1]
      elif End == 'ghost':        
        a = currentGameState.getGhostPosition(agentIndex)
        if node[0][0]==int(a[0]) and node[0][1] == int(a[1]):
          return node[1]  
    else:
      print 'queue empty!!'
      return 1e9

    for a in around:      
      # print (node[0][0]+a[0],node[0][1]+a[1])
      if not currentGameState.hasWall(node[0][0]+a[0],node[0][1]+a[1]) \
        and (node[0][0]+a[0],node[0][1]+a[1]) not in visted:
        queue.append(((node[0][0]+a[0],node[0][1]+a[1]),node[1]+1))
        visted.append((node[0][0]+a[0],node[0][1]+a[1]))

# Abbreviation
better = betterEvaluationFunction


